/*
Navicat MySQL Data Transfer

Source Server         : myconn
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : actionmall

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2019-03-25 13:34:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `action_address`
-- ----------------------------
DROP TABLE IF EXISTS `action_address`;
CREATE TABLE `action_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  `name` varchar(20) NOT NULL COMMENT '收件人姓名',
  `phone` varchar(20) DEFAULT NULL COMMENT '收件人固定电话号码',
  `mobile` varchar(20) NOT NULL COMMENT '收件人手机号码',
  `province` varchar(20) NOT NULL COMMENT '省份',
  `city` varchar(20) NOT NULL COMMENT '城市',
  `district` varchar(20) DEFAULT NULL COMMENT '区/县',
  `addr` varchar(300) DEFAULT NULL COMMENT '详细地址',
  `zip` varchar(6) DEFAULT NULL COMMENT '邮编',
  `default_addr` tinyint(1) DEFAULT '0' COMMENT '是否是默认地址，0-非默认，1-默认',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  `del_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除，0：正常，1：已删除',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `action_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `action_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_address
-- ----------------------------
INSERT INTO `action_address` VALUES ('4', '5', '李四', null, '18966336652', '山东省', '烟台市', '芝罘区', '魁玉路100号', '264001', '0', '2018-02-19 19:12:28', '2019-03-16 08:55:32', '1');
INSERT INTO `action_address` VALUES ('5', '5', '张三天', null, '12345678000', '山东省', '烟台市', '芝罘区', '魁玉路100号', '264000', '0', '2018-02-19 19:12:36', '2019-03-22 11:33:51', '0');
INSERT INTO `action_address` VALUES ('7', '5', '李思城', null, '13356899978', '天津市', '天津市市辖区', '南开区', '天赐龙城12-2', '255668', '0', '2019-03-15 14:52:53', '2019-03-16 12:06:54', '0');
INSERT INTO `action_address` VALUES ('8', '5', '梁静茹', null, '15563876666', '江西省', '南昌市', '东湖区', '前进路100号', '568899', '1', '2019-03-15 15:53:03', '2019-03-22 11:33:51', '0');
INSERT INTO `action_address` VALUES ('9', '5', '张媛媛', null, '13333334455', '北京市', '北京市市辖区', '东城区', '南山路78-2', '', '0', '2019-03-16 09:55:21', '2019-03-16 09:55:27', '1');
INSERT INTO `action_address` VALUES ('10', '5', '填好', null, '11111111111', '河北省', '秦皇岛市', '山海关区', '南山', '', '0', '2019-03-16 12:06:39', '2019-03-16 12:06:51', '1');
INSERT INTO `action_address` VALUES ('11', '5', '张三丰', null, '12222222222', '山东省', '烟台市', '龙口市', '南山公元10-1', '', '1', '2019-03-16 15:42:09', '2019-03-16 15:42:21', '1');

-- ----------------------------
-- Table structure for `action_carts`
-- ----------------------------
DROP TABLE IF EXISTS `action_carts`;
CREATE TABLE `action_carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) NOT NULL COMMENT '用户Id',
  `product_id` int(11) NOT NULL COMMENT '商品Id',
  `quantity` int(11) NOT NULL COMMENT '商品数量',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  `checked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1：选中，0：未选中',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `action_carts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `action_users` (`id`),
  CONSTRAINT `action_carts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `action_products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_carts
-- ----------------------------

-- ----------------------------
-- Table structure for `action_orders`
-- ----------------------------
DROP TABLE IF EXISTS `action_orders`;
CREATE TABLE `action_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `order_no` bigint(20) NOT NULL COMMENT '订单编号',
  `uid` int(11) NOT NULL COMMENT '用户编号',
  `addr_id` int(11) NOT NULL COMMENT '收货地址编号',
  `amount` decimal(20,2) NOT NULL COMMENT '订单付款金额',
  `type` int(11) NOT NULL COMMENT '付款类型，1-在线支付，2-货到付款',
  `freight` int(11) NOT NULL COMMENT '运费',
  `status` int(11) NOT NULL COMMENT '订单状态，1-未付款，2-已付款，3-已发货，4-交易成功，5-交易关闭，6-已取消，',
  `payment_time` datetime DEFAULT NULL COMMENT '支付时间',
  `delivery_time` datetime DEFAULT NULL COMMENT '发货时间',
  `finish_time` datetime DEFAULT NULL COMMENT '交易完成时间',
  `close_time` datetime DEFAULT NULL COMMENT '交易关闭时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `addr_id` (`addr_id`),
  CONSTRAINT `action_orders_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `action_users` (`id`),
  CONSTRAINT `action_orders_ibfk_2` FOREIGN KEY (`addr_id`) REFERENCES `action_address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_orders
-- ----------------------------
INSERT INTO `action_orders` VALUES ('8', '1519095514229', '2', '4', '863.56', '1', '0', '3', null, '2018-02-20 21:37:05', null, null, '2018-02-20 21:37:05', '2018-02-20 10:58:34');
INSERT INTO `action_orders` VALUES ('9', '1519099875344', '2', '4', '863.56', '1', '0', '1', null, null, null, null, '2018-02-20 12:11:15', '2018-02-20 12:11:15');
INSERT INTO `action_orders` VALUES ('10', '1552995071508', '5', '5', '647.67', '1', '0', '1', null, null, null, null, '2019-03-19 19:31:11', '2019-03-19 19:31:11');
INSERT INTO `action_orders` VALUES ('11', '1553226067154', '5', '5', '1500000.00', '1', '0', '1', null, null, null, null, '2019-03-22 11:41:07', '2019-03-22 11:41:07');

-- ----------------------------
-- Table structure for `action_order_items`
-- ----------------------------
DROP TABLE IF EXISTS `action_order_items`;
CREATE TABLE `action_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单项id',
  `uid` int(11) NOT NULL COMMENT '用户编号',
  `order_no` bigint(20) NOT NULL COMMENT '所属订单编号',
  `goods_id` int(11) NOT NULL COMMENT '商品编号',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `icon_url` varchar(500) NOT NULL COMMENT '商品主图',
  `price` decimal(20,2) NOT NULL COMMENT '商品单价',
  `quantity` int(11) NOT NULL COMMENT '购买的商品数量',
  `total_price` decimal(20,2) NOT NULL COMMENT '订单项总价格',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `goods_id` (`goods_id`),
  CONSTRAINT `action_order_items_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `action_users` (`id`),
  CONSTRAINT `action_order_items_ibfk_2` FOREIGN KEY (`goods_id`) REFERENCES `action_products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_order_items
-- ----------------------------
INSERT INTO `action_order_items` VALUES ('19', '2', '1519095514229', '6', '锂基脂 00#', '[地址1', '215.89', '2', '431.78', '2018-02-20 10:58:34', '2018-02-20 10:58:34');
INSERT INTO `action_order_items` VALUES ('20', '2', '1519095514229', '7', '锂基脂 00#', '[地址1', '215.89', '2', '431.78', '2018-02-20 10:58:34', '2018-02-20 10:58:34');
INSERT INTO `action_order_items` VALUES ('21', '2', '1519099875344', '7', '锂基脂 00#', '[地址1', '215.89', '2', '431.78', '2018-02-20 12:11:15', '2018-02-20 12:11:15');
INSERT INTO `action_order_items` VALUES ('22', '2', '1519099875344', '6', '锂基脂 00#', '[地址1', '215.89', '2', '431.78', '2018-02-20 12:11:15', '2018-02-20 12:11:15');
INSERT INTO `action_order_items` VALUES ('23', '5', '1552995071508', '6', '锂基脂 00#', '/upload/d88b7591-47f5-4125-b9a6-3ad29c74cba3.png', '215.89', '3', '647.67', '2019-03-19 19:31:11', '2019-03-19 19:31:11');
INSERT INTO `action_order_items` VALUES ('24', '5', '1553226067154', '10', '壁挂臂级起重机', '/upload/68fb85a6-48f3-4e46-967d-965a51ca22a3.jpg', '1500000.00', '1', '1500000.00', '2019-03-22 11:41:07', '2019-03-22 11:41:07');

-- ----------------------------
-- Table structure for `action_params`
-- ----------------------------
DROP TABLE IF EXISTS `action_params`;
CREATE TABLE `action_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '类别ID',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父类ID，id为0时为根结点，为一类节点',
  `name` varchar(50) NOT NULL COMMENT '类别名称',
  `sort_order` int(11) DEFAULT NULL COMMENT '同类展示顺序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态编码，1有效，0无效',
  `level` int(11) DEFAULT NULL COMMENT '节点级别,顶层节点为0，依次类推',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10061 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_params
-- ----------------------------
INSERT INTO `action_params` VALUES ('10023', '0', '混凝土机械', '1', '1', '0', '2018-02-12 15:44:18', '2018-02-12 15:44:18');
INSERT INTO `action_params` VALUES ('10024', '0', '建筑起重机械', '2', '1', '0', '2018-02-12 15:44:33', '2018-02-12 15:44:33');
INSERT INTO `action_params` VALUES ('10025', '0', '路面机械', '3', '1', '0', '2018-02-12 15:44:43', '2018-02-12 15:44:43');
INSERT INTO `action_params` VALUES ('10026', '0', '土方机械', '4', '1', '0', '2018-02-12 15:44:53', '2018-02-12 15:44:53');
INSERT INTO `action_params` VALUES ('10027', '0', '环卫机械', '5', '1', '0', '2018-02-12 15:45:02', '2018-02-12 15:45:02');
INSERT INTO `action_params` VALUES ('10028', '0', '工业车辆', '6', '1', '0', '2018-02-12 15:45:13', '2018-02-12 15:45:13');
INSERT INTO `action_params` VALUES ('10029', '0', '模型专区', '7', '1', '0', '2018-02-12 15:45:22', '2018-02-12 15:45:22');
INSERT INTO `action_params` VALUES ('10030', '0', '特辑专区', '8', '1', '0', '2018-02-12 15:45:36', '2018-02-12 15:45:36');
INSERT INTO `action_params` VALUES ('10031', '0', '运费-01', '9', '1', '0', '2018-02-12 15:45:43', '2018-02-12 15:52:12');
INSERT INTO `action_params` VALUES ('10032', '10023', '泵送搅拌系统', '1', '1', '1', '2018-02-12 15:46:58', '2018-02-12 15:46:58');
INSERT INTO `action_params` VALUES ('10033', '10023', '油品', '2', '1', '1', '2018-02-12 15:47:08', '2018-02-12 15:47:08');
INSERT INTO `action_params` VALUES ('10034', '10023', '电器元件', '3', '1', '1', '2018-02-12 15:47:17', '2018-02-12 15:47:17');
INSERT INTO `action_params` VALUES ('10035', '10023', '地盘配件', '4', '1', '1', '2018-02-12 15:47:28', '2018-02-12 15:47:28');
INSERT INTO `action_params` VALUES ('10036', '10023', '发动机件', '5', '1', '1', '2018-02-12 15:47:42', '2018-02-12 15:47:42');
INSERT INTO `action_params` VALUES ('10037', '10023', '轮胎', '6', '1', '1', '2018-02-12 15:47:50', '2018-02-12 15:47:50');
INSERT INTO `action_params` VALUES ('10038', '10032', '管阀类', '1', '1', '2', '2018-02-12 15:48:26', '2018-02-12 15:48:26');
INSERT INTO `action_params` VALUES ('10039', '10032', '易损类', '2', '1', '2', '2018-02-12 15:48:41', '2018-02-12 15:48:41');
INSERT INTO `action_params` VALUES ('10040', '10033', '防冻液', '1', '1', '2', '2018-02-12 15:49:07', '2018-02-12 15:49:07');
INSERT INTO `action_params` VALUES ('10041', '10033', '齿轮油', '2', '1', '2', '2018-02-12 15:49:17', '2018-02-12 15:49:17');
INSERT INTO `action_params` VALUES ('10042', '10033', '润滑油', '3', '1', '2', '2018-02-12 15:49:26', '2018-02-12 15:49:26');
INSERT INTO `action_params` VALUES ('10043', '10033', '液压油', '4', '1', '2', '2018-02-12 15:49:35', '2018-02-12 15:49:35');
INSERT INTO `action_params` VALUES ('10044', '10033', '锂基油', '5', '1', '2', '2018-02-12 15:49:58', '2018-02-12 15:49:58');
INSERT INTO `action_params` VALUES ('10045', '10034', '接触器', '1', '1', '2', '2018-02-12 15:50:35', '2018-02-12 15:50:35');
INSERT INTO `action_params` VALUES ('10046', '10034', '开关', '2', '1', '2', '2018-02-12 15:50:43', '2018-02-12 15:50:43');
INSERT INTO `action_params` VALUES ('10047', '10034', '继电器', '3', '1', '2', '2018-02-12 15:50:50', '2018-02-12 15:50:50');
INSERT INTO `action_params` VALUES ('10048', '10034', '遥控器', '4', '1', '2', '2018-02-12 15:50:56', '2018-02-12 15:50:56');
INSERT INTO `action_params` VALUES ('10049', '10034', '断路器', '5', '1', '2', '2018-02-12 15:51:17', '2018-02-12 15:51:17');
INSERT INTO `action_params` VALUES ('10050', '10034', '控制器', '6', '1', '2', '2018-02-12 15:51:26', '2018-02-12 15:51:26');
INSERT INTO `action_params` VALUES ('10051', '10024', '轮胎起重机', '1', '1', '1', '2018-02-12 17:06:24', '2018-02-12 17:06:24');
INSERT INTO `action_params` VALUES ('10052', '10028', '内燃平衡重叉车', '1', '1', '1', '2018-02-12 17:07:39', '2018-02-12 17:07:39');
INSERT INTO `action_params` VALUES ('10053', '10052', 'Z系列', '1', '1', '2', '2018-02-12 17:08:05', '2018-02-12 17:08:05');
INSERT INTO `action_params` VALUES ('10054', '10052', 'E系列', '2', '1', '2', '2018-02-12 17:08:15', '2018-02-12 17:08:15');
INSERT INTO `action_params` VALUES ('10059', '10031', '免运费', '0', '1', '1', '2019-03-07 11:00:52', '2019-03-07 11:00:52');
INSERT INTO `action_params` VALUES ('10060', '10038', 'pvc管阀', '0', '1', '3', '2019-03-21 15:17:47', '2019-03-21 15:17:47');

-- ----------------------------
-- Table structure for `action_products`
-- ----------------------------
DROP TABLE IF EXISTS `action_products`;
CREATE TABLE `action_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `name` varchar(100) NOT NULL COMMENT '商品名称(配件)',
  `product_id` int(11) NOT NULL COMMENT '产品类型编号，对应action_params表中parent_id为0的分类',
  `parts_id` int(11) NOT NULL COMMENT '配件分类,对应action_params表中parent_id非0参数',
  `icon_url` varchar(500) DEFAULT NULL COMMENT '商品主图片',
  `sub_images` text COMMENT '图片地址，一组小图',
  `detail` text COMMENT '商品详情',
  `spec_param` text COMMENT '规格参数',
  `price` decimal(20,2) NOT NULL COMMENT '价格：单位元，保留两位小数',
  `stock` int(11) NOT NULL COMMENT '库存',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '商品的状态：1-待售，刚保存；2-上架，在售；3-下架，停售',
  `is_hot` int(11) NOT NULL DEFAULT '2' COMMENT '是否热销，1-是，2-否',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `parts_id` (`parts_id`),
  CONSTRAINT `action_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `action_params` (`id`),
  CONSTRAINT `action_products_ibfk_2` FOREIGN KEY (`parts_id`) REFERENCES `action_params` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_products
-- ----------------------------
INSERT INTO `action_products` VALUES ('6', '锂基脂 00#', '10023', '10044', '/upload/d88b7591-47f5-4125-b9a6-3ad29c74cba3.png', '/upload/d88b7591-47f5-4125-b9a6-3ad29c74cba3.png', '<p>商品详情</p>', '规格参数', '215.89', '93', '2', '1', '2018-02-20 10:55:52', '2019-03-21 17:55:42');
INSERT INTO `action_products` VALUES ('7', '锂基脂 01#', '10023', '10044', '/upload/9106771f-7ae7-46e6-acb1-c63c991ed40e.png', '/upload/9106771f-7ae7-46e6-acb1-c63c991ed40e.png', '<p>商品详情</p>', '规格参数', '215.89', '96', '2', '1', '2018-02-20 10:55:55', '2019-03-21 17:55:44');
INSERT INTO `action_products` VALUES ('8', '钢架10#', '10023', '10038', '/upload/7ddddb12-6f53-464d-8b6d-4c38e5f70467.png', '/upload/09478142-6a69-4836-bee6-0d6aa158cf0b.png', '<p>商品详情<br></p>', null, '11.00', '21', '3', '2', '2019-02-27 17:05:52', '2019-03-21 15:23:33');
INSERT INTO `action_products` VALUES ('9', '水果运费', '10031', '10059', '/upload/23fd7cab-dc9c-4c67-8191-9e9903b3e25b.png', '/upload/23fd7cab-dc9c-4c67-8191-9e9903b3e25b.png,/upload/23fd7cab-dc9c-4c67-8191-9e9903b3e25b.png,/upload/15d56019-0398-4ce3-bdd5-418fd4c8f9ea.jpg,/upload/eb0dfa8e-4a01-44fe-be90-e8f35a422dbb.jpg', '<p>免运费</p>', null, '0.00', '1000', '1', '2', '2019-03-07 11:01:41', '2019-03-21 19:04:43');
INSERT INTO `action_products` VALUES ('10', '壁挂臂级起重机', '10024', '10051', '/upload/68fb85a6-48f3-4e46-967d-965a51ca22a3.jpg', '/upload/e1ae9346-2076-4115-b295-e30ca2ce6be1.png,/upload/c8b0f45b-1416-475c-9285-de59042d1cb8.png', '<p>起重机<br></p>', null, '1500000.00', '10', '2', '1', '2019-03-19 18:14:09', '2019-03-22 11:41:07');
INSERT INTO `action_products` VALUES ('11', '轮胎起重机F#0090型', '10024', '10051', '/upload/4f5e1694-2f9f-41aa-9e25-e4a7b3bbeb30.jpg', null, '', null, '250000.00', '2', '2', '1', '2019-03-21 15:33:21', '2019-03-21 19:05:33');

-- ----------------------------
-- Table structure for `action_users`
-- ----------------------------
DROP TABLE IF EXISTS `action_users`;
CREATE TABLE `action_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码，MD5加密',
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `question` varchar(50) DEFAULT NULL COMMENT '密码问题',
  `asw` varchar(50) DEFAULT NULL COMMENT '找回密码答案',
  `role` int(11) NOT NULL COMMENT '角色2-管理员，1-普通用户',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `age` int(11) NOT NULL DEFAULT '20' COMMENT '年龄，在0到120之间取值',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别，1：男、0：女',
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '正常0、删除1',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of action_users
-- ----------------------------
INSERT INTO `action_users` VALUES ('1', 'admin', '1234', '1', '1', '1', '1', '1', '2018-02-03 23:35:34', '2018-02-03 23:35:37', '0', '0', '0', '');
INSERT INTO `action_users` VALUES ('2', 'admin01', '81dc9bdb52d04dc20036dbd8313ed055', 'action@action.cn', '123456789', '这是什么问题？', '不知道', '2', '2018-02-06 23:23:54', '2018-02-08 22:37:09', '0', '0', '0', '');
INSERT INTO `action_users` VALUES ('3', 'action01', '202cb962ac59075b964b07152d234b70', 'action01@action.cn', '1556688999', '你的密码是什么', '不告诉你', '1', '2018-02-08 14:49:33', '2019-03-11 15:29:39', '23', '0', '0', null);
INSERT INTO `action_users` VALUES ('4', 'action02', '202cb962ac59075b964b07152d234b70', 'action02@action.cn', '123456789', '你的密码是什么', '不告诉你', '1', '2018-02-08 14:53:16', '2019-03-11 15:38:47', '20', '0', '0', '沈辉');
INSERT INTO `action_users` VALUES ('5', 'zhangsan', '96e79218965eb72c92a549dd5a330112', 'zhangsan@sohu.com', '13356896698', 'my name', 'zhang', '1', '2019-03-12 09:19:06', '2019-03-18 15:06:53', '35', '1', '0', '张三');
INSERT INTO `action_users` VALUES ('6', 'lisi', '96e79218965eb72c92a549dd5a330112', 'lisi@163.com', '12222222222', 'my name', 'lisi', '1', '2019-03-12 09:22:29', '2019-03-12 09:22:29', '20', '1', '0', null);
