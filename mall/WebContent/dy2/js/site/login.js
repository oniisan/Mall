define(['common'],function(common){
	var isAccountValidate=false;
	var isPasswordValidate=false;
	//失去光标验证用户名
	function accountCheck(){
		$("#username").blur(function(){
			isAccountValidate=checkAccount();
		});
	}
	
	//失去光标验证密码
	function passwordCheck(){
		$("#password").blur(function(){
			isPasswordValidate=checkPassword();
		});
	}
	
	
	//登录
	function loginBtn(){
		//创建点击事件
		$(".login_btn").click(function(){
			//判断验证信息
			if(!isAccountValidate){
				return checkAccount();
			}
			if(!isPasswordValidate){
				return checkPassword();
			}
			//成功登录
			$.ajax({
				url:baseUrl+"user/do_login.do",
				type:"post",
				data:{account:$("#username").val(),
					  password:$("#password").val()},
				xhrFields:{withCredentials:true},//允许跨域请求携带cookie数据
				crossDomain:true,//跨域请求
				success:function(data){
					//判断是否成功
					
//					console.log("000000000000000");
					
					if(data.status==0){
						//判断是否为管理员
						if(data.data.role==2){
							$(window).attr("location","../admin/index.html");
						}else{
							$(window).attr("location","index.html");
						}
					}else{
						$("#passwordError").css({display:"block"});
						$("#passwordError").html(data.msg);
					}
				}
			});
				
				
				
		});
	}
	
    //校验用户名
	function checkAccount(){
		//获取用户名
		var account=$("#username").val();
		$("#usernameError").css({display:"none"});
		if(account==""){
			$("#usernameError").css({display:"block"});
			$("#usernameError").html("用户名不能为空");
			return false;
		}
		return true;
	}
	
	//校验密码
	function checkPassword(){
		var pwd=$("#password").val();
		$("#passwordError").css({display:"none"});
		if(pwd==""){
			$("#passwordError").css({display:"block"});
			$("#passwordError").html("密码不能为空");
			return false;
		}
		return true;
	}
	
	
	return{
		accountCheck:accountCheck,
		passwordCheck:passwordCheck,
		loginBtn:loginBtn
	};
	
	
});