package cn.techaction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.techaction.common.SverResponse;
import cn.techaction.pojo.ActionProduct;
import cn.techaction.service.ActionProductService;
import cn.techaction.utils.PageBean;
import cn.techaction.vo.ActionProductFloorVo;
import cn.techaction.vo.ActionProductListVo;

@Controller
@RequestMapping("/product")
public class ActionProductController {
//	创建service对象
	@Autowired
	private ActionProductService actionProductService;
	@RequestMapping("/find_product.do")
	@ResponseBody
	//查询商品信息根据商品类型和配件类型查询以及总页数与每页行数
	public SverResponse<PageBean<ActionProduct>> findProducts(Integer productId, Integer partId
			,Integer pageNum,Integer pageSize) {
		// TODO Auto-generated method stub
		//调用Service层方法进行分页查询
		return actionProductService.findProduct(productId, partId, pageNum, pageSize);
	}
	/**
	 * 热销商品展示
	 * @param num
	 * @return
	 */
	@RequestMapping("/findhotproducts.do")
	@ResponseBody
	public SverResponse<List<ActionProduct>> findHotProducts(Integer num){
		return actionProductService.findHotProducts(num);
	}
	/**
	 * 查找楼层商品
	 * @return
	 */
	@RequestMapping("/findfloors.do")
	@ResponseBody
	public SverResponse<ActionProductFloorVo> findFloorProduct(){
		return actionProductService.findFloorProducts();
		
	}
	/**
	 * 门户，根据商品编号获得商品信息
	 * @return
	 */
	@RequestMapping("/getdetail.do")
	@ResponseBody
	public SverResponse<ActionProduct> getProductDetail(Integer productId){
		return actionProductService.findProductDetailForPortal(productId);
	}
	/**
	 * 查询商品信息
	 * @param productTypeId
	 * @param partsId
	 * @param name
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/searchproducts.do")
	@ResponseBody
	public SverResponse<PageBean<ActionProductListVo>> searchProducts(Integer productTypeId,
			Integer partsId,String name,
			@RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
			@RequestParam(value ="pageSize",defaultValue="10") int pageSize){
		return actionProductService.findProductsForProtal(productTypeId,partsId,name,pageNum,pageSize);
	}
}
