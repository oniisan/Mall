package cn.techaction.service;

import cn.techaction.common.SverResponse;
import cn.techaction.vo.ActionCartVo;

public interface ActionCartService {
	/**
	 * 保存商品信息至购物车
	 * @param id
	 * @param productId
	 * @param count
	 * @return
	 */
	public SverResponse<String> saveOrUpdate(Integer userid, Integer productId, Integer count);
	/**
	 * 查看购物车所有信息
	 * @param id
	 * @return
	 */
	public SverResponse<ActionCartVo> findAllCarts(Integer userid);
	/**
	 * 清空购物车
	 * @param id
	 * @return
	 */
	public SverResponse<String> clearCart(Integer userid);
	/**
	 * 更新购物车信息
	 * @param id
	 * @param productId
	 * @param count
	 * @param checked
	 * @return
	 */
	public SverResponse<ActionCartVo> updateCart(Integer userid, Integer productId, Integer count, Integer checked);
	/**
	 * 删除商品
	 * @param id
	 * @param productId
	 * @return
	 */
	public SverResponse<ActionCartVo> deleteCart(Integer userid, Integer productId);
	/**
	 * 获得购物车中数量
	 * @param id
	 * @return
	 */
	public SverResponse<Integer> getCartCount(Integer userid);

}
