package cn.techaction.service;

import cn.techaction.common.SverResponse;
import cn.techaction.utils.PageBean;
import cn.techaction.vo.ActionOrderVo;

public interface ActionOrderService {
/**
 * 订单分页列表
 * @param id
 * @param status
 * @param pageNum
 * @param pageSize
 * @return
 */
	public SverResponse<PageBean<ActionOrderVo>> findOrder(Integer userid, Integer status, int pageNum, int pageSize);
/**
 * 取消订单
 * @param id
 * @param orderNo
 * @return
 */
public SverResponse<String> cancelOrder(Integer id, Long orderNo);
/**
 * 根据订单号获取订单详情
 * @param id
 * @param orderNo
 * @return
 */
public SverResponse<ActionOrderVo> findOrderDetail(Integer userid, Long orderNo);
/**
 * 创建订单
 * @param userid
 * @param addrId
 * @return
 */
public SverResponse<ActionOrderVo> generateOrder(Integer userid, Integer addrId);

}
