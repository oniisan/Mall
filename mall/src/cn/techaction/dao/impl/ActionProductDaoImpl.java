package cn.techaction.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import cn.techaction.dao.ActionProductDao;
import cn.techaction.pojo.ActionProduct;

@Repository//数据访问层
public class ActionProductDaoImpl implements ActionProductDao {
	/**
	 * 查询总数量
	 */
	@Autowired //自动创建
	private QueryRunner queryRunner;
	private String alias = "id,name,product_id as productId,"
			+ "parts_id as partsId,icon_url as iconUrl,sub_images as"
			+ " subImages,detail,spec_param as specParam,price,stock"
			+ ",status,is_hot as hot,created,updated";
	@Override
	public Integer getTotalCount(Integer productId, Integer partsId) {
		// TODO Auto-generated method stub
		
		String sql = "select count(*) as num from action_products where 1=1";
		//参数集合
		List<Object> params=new ArrayList<>();
		
		if (productId!=null) {
			sql+=" and product_id = ?";
			params.add(productId);
		}
		if (partsId!=null) {
			sql+=" and parts_id = ?";
			params.add(partsId);
			}
		try {
			
			return queryRunner.query(sql, new ColumnListHandler<Long>("num"),params.toArray()).get(0).intValue();//获取返回list的第0个元素并转化为int型
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 查询具体信息
	 */
	@Override
	public List<ActionProduct> findProductsByInfo(Integer productId, Integer partsId, Integer startIndex,
			Integer pageSzie) {
		// TODO Auto-generated method stub
		String sql="select id,name,product_id as productId,parts_id as partsId,"
				+"icon_url as iconUrl,sub_images as subImages,detail,spec_param as specParam,"
				+"price,stock,status,is_hot as hot,"
				+"created,updated from action_products where 1=1";
		//参数集合
		List<Object> params=new ArrayList<>();
		
		if (productId!=null) {
			sql+=" and product_id = ?";
			params.add(productId);
		}
		if (partsId!=null) {
			sql+=" and parts_id = ?";
			params.add(partsId);
		}
		sql+=" limit ?,?";
		params.add(startIndex);
		params.add(pageSzie);
		try {
			return queryRunner.query(sql,new BeanListHandler<ActionProduct>(ActionProduct.class)
					,params.toArray());
			
		}catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 根据id查询商品信息
	 */
	@Override
	public ActionProduct findProductById(Integer id) {
		String sql="select "+ this.alias+" from action_products where id=?";
		try {
			return queryRunner.query(sql, new BeanHandler<ActionProduct>(ActionProduct.class),id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 更新商品信息
	 */
	@Override
	public int updateProduct(ActionProduct product) {
		String sql="update action_products set updated = ?";
		List<Object> params = new ArrayList<>();
		params.add(product.getupdated());
		if(!StringUtils.isEmpty(product.getName())) {
			sql +=",name = ?";
			params.add(product.getName());
		}
		if(product.getProductId()!=null) {
			sql +=",product_id=?";
			params.add(product.getProductId());
		}
		if(product.getPartsId()!=null) {
			sql +=",parts_id=?";
			params.add(product.getPartsId());
		}
		if(product.getPrice()!=null) {
			sql +=",price = ?";
			params.add(product.getPrice());
		}
		if(product.getStock()!=null) {
			sql +=",stock = ?";
			params.add(product.getStock());
		}
		if(!StringUtils.isEmpty(product.getIconUrl())) {
			sql +=",icon_url = ?";
			params.add(product.getIconUrl());
		}
		if(!StringUtils.isEmpty(product.getSubImages())) {
			sql +=",sub_images = ?";
			params.add(product.getSubImages());
		}
		if(product.getStatus()!=null) {
			sql +=",status = ?";
			params.add(product.getStatus());
		}
		if(!StringUtils.isEmpty(product.getDetail())) {
			sql +=",detail = ?";
			params.add(product.getDetail());
		}
		if(!StringUtils.isEmpty(product.getSpecParam())) {
			sql +=",spec_param = ?";
			params.add(product.getSpecParam());
		}
		if(product.getHot()!=null) {
			sql +=",is_hot = ?";
			params.add(product.getHot());
		}
		sql+= " where id = ?";
		params.add(product.getId());
		
		try {
			return queryRunner.update(sql,params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 情况购物车记录
	 */
	@Override
	public int deleteCartProduct(Integer userid) {
		String sql="delete from action_carts where user_id=?";
		try {
			return queryRunner.update(sql,userid);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 查找热门商品
	 */
	@Override
	public List<ActionProduct> findHotProducts(Integer num) {
		String sql="select "+this.alias+" from action_products where is_hot=1 and status=2 "
				+ "order by updated,id desc";
		if(num !=null) {
			sql+=" limit 0,?";
		}
		try {
			if(num!=null) {
				return queryRunner.query(sql, new BeanListHandler<ActionProduct>(ActionProduct.class), num);
			}else {
				return queryRunner.query(sql, new BeanListHandler<ActionProduct>(ActionProduct.class));
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据商品类型查询商品信息
	 */
	@Override
	public List<ActionProduct> findProductsByProductCategory(int categoryId) {
		String sql = "select " +this.alias+ " from action_products where product_id = ? and status=2 order by updated desc";
		try {
			return queryRunner.query(sql, new BeanListHandler<ActionProduct>(ActionProduct.class), categoryId);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 查询总记录数
	 */
	@Override
	public Integer getTotalCount(ActionProduct product) {
		String sql ="select count(id) as num from action_products where 1=1 ";
		List<Object> params = new ArrayList<>();
		if(product.getId()!=null) {
			sql+=" and id = ?";
			params.add(product.getId());
		}
		if(product.getName()!=null) {
			sql+=" and name like ?";
			params.add("%"+product.getName()+"%");
		}
		if(product.getStatus()!=null) {
			sql+=" and status = ?";
			params.add(product.getStatus());
		}
		if(product.getProductId()!=null) {
			sql+=" and product_id = ?";
			params.add(product.getProductId());
		}
		if(product.getPartsId()!=null) {
			sql+=" and parts_id = ?";
			params.add(product.getPartsId());
		}
		try {
			return queryRunner.query(sql, new ColumnListHandler<Long>("num"), params.toArray()).get(0).intValue();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 根据条件分页查询
	 */
	@Override
	public List<ActionProduct> findProducts(ActionProduct product, int startIndex, int pageSize) {
		String sql = "select " +this.alias+ " from action_products where 1=1";
		List<Object> params = new ArrayList<>();
		if(product.getId()!=null) {
			sql+=" and id = ?";
			params.add(product.getId());
		}
		if(product.getName()!=null) {
			sql+=" and name like ?";
			params.add("%"+product.getName()+"%");
		}
		if(product.getStatus()!=null) {
			sql+=" and status = ?";
			params.add(product.getStatus());
		}
		if(product.getProductId()!=null) {
			sql+=" and product_id = ?";
			params.add(product.getProductId());
		}
		if(product.getPartsId()!=null) {
			sql+=" and parts_id = ?";
			params.add(product.getPartsId());
		}
		sql+=" limit ?,?";
		params.add(startIndex);
		params.add(pageSize);
		try {
			return queryRunner.query(sql, new BeanListHandler<ActionProduct>(ActionProduct.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}


}
