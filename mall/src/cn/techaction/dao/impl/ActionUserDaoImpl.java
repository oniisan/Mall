package cn.techaction.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.springframework.stereotype.Repository;



import cn.techaction.dao.ActionUserDao;
import cn.techaction.pojo.User;

@Repository//数据访问层
public class ActionUserDaoImpl implements ActionUserDao {
	@Resource
	//创建查询对象
	private QueryRunner queryRunner;
	@Override
	public int checkUserByAccount(String account) {
		// TODO Auto-generated method stub
		String sql="select count(*) as num from action_users where account=?";
		try {
			List<Long> rs=queryRunner.query(sql, new ColumnListHandler<Long>("num"),account);
			return rs.size()>0?rs.get(0).intValue():0;
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 根据账号密码查找用户
	 */
	@Override
	public User findUserByAccountAndPassword(String account, String password) {
		// TODO Auto-generated method stub
		String sql="select * from action_users where account=? and password=?";
		try {
			return queryRunner.query(sql, new BeanHandler<User>(User.class),account,password);
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 验证邮箱是否被注册
	 */
	@Override
	public int checkUserByEamil(String email) {
		String sql="select count(account) as num from action_users where email=?";
		try {
			List<Long> rs = queryRunner.query(sql,new ColumnListHandler<Long>("num"), email);
			return rs.size()>0?rs.get(0).intValue():0;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 验证手机号是否被注册
	 */
	@Override
	public int checkUserByPhone(String phone) {
		String sql="select count(account) as num from action_users where phone=?";
		try {
			List<Long> rs = queryRunner.query(sql,new ColumnListHandler<Long>("num"), phone);
			return rs.size()>0?rs.get(0).intValue():0;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 新增用户
	 */
	@Override
	public int insertUser(User user) {
		String sql = "insert into action_users(account,password,email,phone,question,asw,role,create_time,update_time)"
				+ " values(?,?,?,?,?,?,?,?,?)";
		Object[] params = {user.getAccount(),user.getPassword(),user.getEmail(),user.getPhone(),user.getQuestion(),
							user.getAsw(),user.getRole(),user.getCreate_time(),user.getUpData_time()};
		try {
			return queryRunner.update(sql, params);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 根据用户名获取对象
	 */
	@Override
	public User findUserByAccount(String account) {
		String sql = "select * from action_users where account=?";
		try {
			return queryRunner.query(sql, new BeanHandler<User>(User.class), account);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}
	/**
	 * 检查用户密码问题
	 */
	@Override
	public int checkUserAnswer(String account,String question, String asw) {
		String sql = "select count(account) as num from action_users"
				+ " where account=? and question = ? and asw = ?";
		try {
			List<Long> rs = queryRunner.query(sql, new ColumnListHandler<Long>("num"),account,question, asw);
			return rs.size()>0?rs.get(0).intValue():0;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 根据用户id查找信息
	 */
	@Override
	public User findUserById(Integer userId) {
		String sql = "select * from action_users where id=?";
		try {
			return queryRunner.query(sql, new BeanHandler<User>(User.class), userId);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 更新用户信息
	 */
	@Override
	public int updateUserInfo(User user) {
		String sql = "update action_users set update_time = ?,password = ?,email = ?,"
				+ "phone = ?,question = ?,asw = ?,name = ?, age = ?,sex = ?,create_time = ?,"
				+ "account = ?,role = ?,del = ? where id=?";
		List<Object> params = new ArrayList<>();
		params.add(user.getUpData_time());
		params.add(user.getPassword());
		params.add(user.getEmail());
		params.add(user.getPhone());
		params.add(user.getQuestion());
		params.add(user.getAsw());
		params.add(user.getName());
		params.add(user.getAge());
		params.add(user.getSex());
		params.add(user.getCreate_time());
		params.add(user.getAccount());
		params.add(user.getRole());
		params.add(user.getDel());
		params.add(user.getId());
		
		try {
			return queryRunner.update(sql, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 验证密码是否存在
	 */
	@Override
	public int checkPassword(String account, String oldPassword) {
		String sql ="select count(account) as num from action_users where account=? and password=?";
		try {
			List<Long> rs = queryRunner.query(sql, new ColumnListHandler<Long>("num"),account,oldPassword);
			return rs.size()>0?rs.get(0).intValue():0;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
}
