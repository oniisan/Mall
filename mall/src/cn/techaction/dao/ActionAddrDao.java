package cn.techaction.dao;

import java.util.List;

import cn.techaction.pojo.ActionAddress;

public interface ActionAddrDao {
	
	/**
	 * 查询是否存在默认地址
	 * @param userId
	 * @return
	 */
	public int findDefaultAddrByUserId(Integer userId);
	
	/**
	 * 新增收件地址
	 * @param addr
	 * @return
	 */
	public int instertAddress(ActionAddress addr);
	/**
	 * 更新收件地址
	 * @param addr
	 * @return
	 */

	public int updateAddress(ActionAddress addr);
	/**
	 * 查询用户的收件人地址信息
	 * @param userId
	 * @return
	 */
	public List<ActionAddress> findAddressByUserId(Integer userId);
	
	/**
	 * 读取用户默认地址
	 * @param userId
	 * @return
	 */

	public ActionAddress findDefaultAddr(Integer userId);
	/**
	 * 根据id查询收货人地址信息
	 * @param addrId
	 * @return
	 */
	public ActionAddress findAddressById(Integer addrId);

}
