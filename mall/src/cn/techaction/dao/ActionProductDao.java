package cn.techaction.dao;

import java.util.List;

import cn.techaction.pojo.ActionProduct;

public interface ActionProductDao {
	/**
	 * 根据条件查询商品的数量
	 * @param productId
	 * @param partsId
	 * @return
	 */
	public Integer getTotalCount(Integer productId, Integer partsId);
	/**
	 * 根据条件插入商品信息
	 * @param productId
	 * @param partsId
	 * @param pageNum
	 * @param pageSzie
	 * @return
	 */
	public List<ActionProduct> findProductsByInfo(Integer productId, Integer partsId,Integer startIndex,Integer pageSize);
	/**
	 * 根据商品Id查询商品信息
	 * @param productId
	 * @return
	 */
	public ActionProduct findProductById(Integer id);
	/**
	 * 更新商品信息
	 * @param product
	 */
	public int updateProduct(ActionProduct product);
	/**
	 * 删除某个用户购物车所有商品
	 * @param userid
	 */
	public int deleteCartProduct(Integer userid);
	/**
	 * 查找热门商品
	 * @param num
	 * @return
	 */
	public List<ActionProduct> findHotProducts(Integer num);
	/**
	 * 根据商品类型查询商品信息
	 * @param  categoryId
	 * @return
	 */
	public List<ActionProduct> findProductsByProductCategory(int  categoryId);
	/**
	 * 根据条件获得总记录数
	 * @param product
	 * @return
	 */
	public Integer getTotalCount(ActionProduct product);
	/**
	 * 根据条件分页查询
	 * @param product
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public List<ActionProduct> findProducts(ActionProduct product, int startIndex, int pageSize);
}
