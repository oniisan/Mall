package cn.techaction.dao;

import cn.techaction.pojo.User;

public interface ActionUserDao {
	
	/**
	 * 根据用户名查找用户
	 * @param account
	 * @return
	 */
	public int checkUserByAccount(String account);
	/**
	 * 根据用户名和密码查找用户
	 * @param account
	 * @param password
	 * @return
	 */
	public User findUserByAccountAndPassword(String account,String password);
	/**
	 * 验证电子邮箱是否已被注册
	 * @param email
	 * @return
	 */
	public int checkUserByEamil(String email);
	/**
	 * 
	 * @param phone
	 * @return
	 */
	public int checkUserByPhone(String phone);
	/**
	 * 新增用户
	 * @param user
	 * @return
	 */
	public int insertUser(User user);
	/**
	 * 根据用户名获取用户
	 * @param account
	 * @return
	 */
	public User findUserByAccount(String account);
	
	/**
	 * 检查用户密码的答案
	 * @param question
	 * @param asw
	 * @return
	 */
	public int checkUserAnswer(String account, String question, String asw);
	/**
	 * 通过id查找用户信息
	 * @param userId
	 * @return
	 */
	public User findUserById(Integer userId);
	/**
	 * 更细用户信息
	 * @param user
	 * @return
	 */
	public int updateUserInfo(User user);
	/**
	 * 验证用户密码是否存在
	 * @param account
	 * @param oldPassword
	 * @return
	 */
	public int checkPassword(String account, String oldPassword);
}
