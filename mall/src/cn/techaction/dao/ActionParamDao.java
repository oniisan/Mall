package cn.techaction.dao;

import java.util.List;

import cn.techaction.pojo.ActionParam;

public interface ActionParamDao {
	/**
	 * 通过父节点id查找子节点参数
	 * @param i
	 * @return
	 */
	public List<ActionParam> findParamsByParentId(Integer parentId);
	/**
	 * 根据节点id查找参考对象
	 * @param partsId
	 * @return
	 */
	public ActionParam findParamById(Integer id);

}
